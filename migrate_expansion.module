<?php

/**
 * @file
 * Provides utility functions to streamline D7 => D8 migrations.
 */

use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Implements hook_migration_plugins_alter().
 */
function migrate_expansion_migration_plugins_alter(array &$migrations) {
  // Replace default widget settings plugin. It ignores everything that is not
  // related to core, and adding a custom one at the end of the process pipeline
  // is not behaving as expected (it's supposed to get value from the previous
  // step, but that's not what is happening for the 'options/settings' field.
  if (isset($migrations['d7_field_instance_widget_settings'])) {
    $migrations['d7_field_instance_widget_settings']['process']['options/settings']['plugin'] = 'migrate_expansion_field_instance_widget_settings';
  }
}

/**
 * Adjusts the format of a field for a given list of bundles.
 *
 * This is needed to prevent migrate from skipping the field due to a
 * field storage limitation on text formats in which two field instances
 * cannot use different text formats in Drupal 8 but they could in 7.
 *
 * @param \Drupal\migrate\Row $row
 *   The current migrate row.
 * @param \Drupal\migrate\Plugin\MigrationInterface $migration
 *   The migration object being executed.
 * @param string $field_name
 *   The field name to adjust.
 * @param array $bundles
 *   An array of bundles that this field is attached to and need adjusting. If
 *   none are passed, all will be adjusted (recommended).
 *
 * @throws \Exception
 *   If the migration is frozen which forbids altering its data.
 *
 * @see https://www.drupal.org/docs/8/upgrade/known-issues-when-upgrading-from-drupal-6-or-7-to-drupal-8#plain-text
 */
function _migrate_expansion_textfields_set_filtered_text_storage(Row $row, MigrationInterface $migration, $field_name, array $bundles = []) {
  if ($row->getSourceProperty('field_name') !== $field_name) {
    return;
  }

  $instances = $row->getSourceProperty('instances');
  $updated_instances = [];
  foreach ($instances as $field_instances) {
    if (empty($bundles) || in_array($field_instances['bundle'], $bundles)) {
      $data = unserialize($field_instances['data']);

      // Set text processing to filtered text.
      $data['settings']['text_processing'] = 1;
      $field_instances['data'] = serialize($data);
    }
    $updated_instances[] = $field_instances;
  }
  $row->setSourceProperty('instances', $updated_instances);
}

/**
 * Implements hook_migrate_prepare_row().
 */
function migrate_expansion_migrate_prepare_row(Row $row, MigrateSourceInterface $source, MigrationInterface $migration) {
  if ($source->getPluginId() === 'd7_field_instance_per_form_display') {
    _migrate_expansion_map_contrib_field_widgets($row, $source, $migration);
    return;
  }

  $field_migration_plugins = [
    'd7_field',
    'd7_field_instance',
  ];

  if (!in_array($source->getPluginId(), $field_migration_plugins)) {
    return;
  }

  $expansion_settings = \Drupal::config('migrate_expansion.settings');
  if (!$as_filtered_fields = unserialize($expansion_settings->get('d7_textfield_plain_and_filtered_keep_filtered'))) {
    return;
  }

  if (in_array($row->getSourceProperty('field_name'), $as_filtered_fields)) {
    _migrate_expansion_textfields_set_filtered_text_storage($row, $migration, $row->getSourceProperty('field_name'));
  }
}


/**
 * @param \Drupal\migrate\Row $row
 * @param \Drupal\migrate\Plugin\MigrateSourceInterface $source
 * @param \Drupal\migrate\Plugin\MigrationInterface $migration
 */
function _migrate_expansion_map_contrib_field_widgets(Row $row, MigrateSourceInterface $source, MigrationInterface $migration) {
  // Map field widgets to their respective D8 plugins.
  $field_widget_mappings = [
    // Numberfield from html5_tools.
    'numberfield' => 'number',
    'inline_entity_form_single' => 'inline_entity_form_simple',
  ];

  $widget = $row->getSourceProperty('widget');

  foreach ($field_widget_mappings as $source_widget_type => $destination_widget_type) {
    if ($widget['type'] == $source_widget_type) {
      $widget['type'] = $destination_widget_type;
      $row->setSourceProperty('widget', $widget);
    }
  }
}