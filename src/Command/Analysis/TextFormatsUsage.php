<?php

namespace Drupal\migrate_expansion\Command\Analysis;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drush\Commands\DrushCommands;

/**
 * Class TextFormatsUsage.
 */
class TextFormatsUsage extends DrushCommands {

  /**
   * Returns a list of available text formats as well as how much they're used.
   *
   * @param string $legacy_db_key .
   *   The database connection key for the D7 database.
   *
   * @option $format
   *   The format to print the data to. Don't pass anything to show a table.
   *   Accepts any of the Drush's built-in formats.
   *
   * @usage migrate_expansion:analysis:text-formats-usage $legacy_key
   *   --format=csv
   * @usage migrate_expansion:analysis:text-formats-usage $legacy_key
   *
   * @command migrate_expansion:analysis:text-formats-usage
   *
   * @return array|void
   */
  public function textFormatsUsage($legacy_db_key, $options = ['format' => NULL]) {
    $connection = Database::getConnection('default', $legacy_db_key);

    $used_formats = [];
    $text_formats = $this->getAvailableTextFormats($connection);
    $table_column_map = $this->getTextfieldTablesColumnMap($connection);

    $table = [];
    foreach ($text_formats as $item) {
      foreach ($table_column_map as $entry) {
        $table_format_count = $connection->query("SELECT count(*) FROM {$entry->TABLE_NAME}
          WHERE {$entry->COLUMN_NAME} = :format",
          [
            ':format' => $item->format,
          ])->fetchField();
        $total_table_count = $connection->query("SELECT count(*) FROM {$entry->TABLE_NAME}")->fetchField();

        if ($table_format_count != 0) {
          $table[] = [
            $item->format, $entry->TABLE_NAME, $table_format_count, $total_table_count,
          ];

          if (!in_array($item->format, $used_formats)) {
            $used_formats[] = $item->format;
          }
        }
      }
    }

    foreach (array_diff(array_keys($text_formats), $used_formats) as $unused_format) {
      $table[] = [$unused_format, 'UNUSED', 'UNUSED', 'UNUSED'];
    }

    $headers = ['Text Format', 'Field Table', 'Format use count', 'Total table rows'];
    // No format specified. Print table.
    if (empty($options['format'])) {
      $this->io()->table($headers, $table);
      return;
    }

    array_unshift($table, $headers);
    return $table;
  }

  /**
   * Gets the available text formats on the Drupal 7 site.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *
   * @return mixed
   */
  protected function getAvailableTextFormats(Connection $connection) {
    $fields = $connection->query("SELECT * FROM {filter_format}")
      ->fetchAllAssoc('format');

    return $fields;
  }

  /**
   * Gets a map of table_name + column_name for text-supporting field tables.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *
   * @return mixed
   */
  protected function getTextfieldTablesColumnMap(Connection $connection) {
    $database_name = $connection->getConnectionOptions()['database'];
    $table_column_map = $connection->query("SELECT TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA='{$database_name}' AND COLUMN_NAME LIKE '%_format'")
      ->fetchAll();
    return $table_column_map;
  }

}
