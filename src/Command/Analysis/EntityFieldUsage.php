<?php

namespace Drupal\migrate_expansion\Command\Analysis;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drush\Commands\DrushCommands;

/**
 * Class EntityFieldUsage.
 */
class EntityFieldUsage extends DrushCommands {

  /**
   * Renders a list of field usage data per entity type and bundle.
   *
   * @param string $legacy_db_key .
   *   The database connection key for the D7 database.
   * @param string $entity_type
   *   The entity type for which to retrieve field usage data.
   *
   * @option $format
   *   The format to print the data to. Don't pass anything to show a table.
   *   Accepts any of the Drush's built-in formats.
   *
   * @usage migrate_expansion:analysis:entity-field-usage $legacy_key $entity_type
   *   --format=csv
   * @usage migrate_expansion:analysis:entity-field-usage $legacy_key $entity_type
   *
   * @command migrate_expansion:analysis:entity-field-usage
   *
   * @return array|void
   */
  public function entityFieldUsage($legacy_db_key, $entity_type, $options = ['format' => NULL]) {
    $connection = Database::getConnection('default', $legacy_db_key);

    $field_list = $this->getFieldsPerEntityType($connection, $entity_type);

    $headers = ['Entity Type', 'Bundle', 'Field Name', 'Count'];
    $table = [];

    foreach ($field_list as $item) {
      $field_count = $connection->query("SELECT count(*) FROM field_data_{$item->field_name} fd
        WHERE fd.bundle = :bundle",
        [
          ':bundle' => $item->bundle,
        ])
        ->fetchField();

      $table[] = [
        $entity_type, $item->bundle, $item->field_name, $field_count,
      ];
    }

    // No format specified. Print table.
    if (empty($options['format'])) {
      $this->io()->table($headers, $table);
      return;
    }

    array_unshift($table, $headers);
    return $table;
  }

  /**
   * @param \Drupal\Core\Database\Connection $connection
   *
   * @return mixed
   */
  protected function getFieldsPerEntityType(Connection $connection, string $entity_type) {
    $fields = $connection->query("SELECT fci.field_name, fci.entity_type, fci.bundle FROM field_config_instance fci
      WHERE fci.entity_type = :entity_type
      ORDER BY fci.bundle ASC",
      [
        ':entity_type' => $entity_type,
      ])
      ->fetchAll();

    return $fields;
  }

}
