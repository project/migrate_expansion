<?php

namespace Drupal\migrate_expansion\Command\Analysis;

use Drupal\Core\Database\Database;
use Drush\Commands\DrushCommands;

/**
 * Class ParagraphsHierarchy.
 */
class ParagraphsHierarchy extends DrushCommands {

  private $paragraphsFields;

  /**
   * @param string $legacy_db_key .
   *   The database connection key for the D7 database.
   * @option $format
   *   The format to print the data to. Don't pass anything to show a table.
   *   Accepts any of the Drush's built-in formats.
   *
   * @usage migrate_expansion:analysis:paragraphs-hierarchy $legacy_key
   *   --format=csv
   * @usage migrate_expansion:analysis:paragraphs-hierarchy $legacy_key
   *
   * @command migrate_expansion:analysis:paragraphs-hierarchy
   *
   * @return array|void
   */
  public function paragraphsHierarchy($legacy_db_key, $options = ['format' => NULL]) {
    $connection = Database::getConnection('default', $legacy_db_key);

    $paragraph_field_instances = $this->getParagraphFieldsInstances($connection);
    $this->paragraphsFields = $paragraph_field_instances;

    // The tree starts at nodes (yes, this is an assumption), so keep only
    // paragraph fields referenced from 'node' entity types. The info for the
    // other paragraph types will be stored in a tree structure.
    $top_level_nodes = [];
    foreach ($paragraph_field_instances as $data) {
      if ($data->entity_type != 'node') {
        continue;
      }
      $item = [
        'bundle' => $data->bundle,
      ];
      $item['children'] = $this->getParagraphsInstanceChildren($data);
      $top_level_nodes[$data->bundle] = $item;
    }

    $current_branch = '';
    $branches = [];
    foreach ($top_level_nodes as $tree_node) {
      $this->getParagraphsUsageTreeArray($tree_node, $current_branch, $branches);
      $current_branch = '';
    }

    // Prepare collection headers.
    $levels = count(max($branches));
    $headers = [];
    for ($level = 1; $level <= $levels; $level++) {
      $headers[] = 'Level ' . $level;
    }

    // No format specified. Print table.
    if (empty($options['format'])) {
      $this->io()->table($headers, $branches);
      return;
    }

    array_unshift($branches, $headers);
    return $branches;
  }

  /**
   * Gets a tree representing the hierarchy of paragraphs, as an array.
   *
   * Each entry of the main array, represents a branch, and it's another array
   * containing all the nodes in that branch, from the root, to the leaf.
   *
   * @param array $node
   *   Represents a tree node. It contains a 'bundle' property, and 'children',
   *   if it's a non-leaf node.
   * @param string $current_branch
   *   A string representation of the branch being explored so far.
   * @param array $branches
   *   The total of branches gathered so far.
   */
  public function getParagraphsUsageTreeArray($node, &$current_branch, &$branches) {
    if ($node) {
      $current_branch .= empty($current_branch) ? $node['bundle'] : ' --> ' . $node['bundle'];
    }

    if (isset($node['children'])) {
      foreach ($node['children'] as $child) {
        $next_current = $current_branch;
        $this->getParagraphsUsageTreeArray($child, $next_current, $branches);
      }
    }
    if (!isset($node['children'])) {
      $branches[] = explode(' --> ', $current_branch);
    }
  }

  /**
   * Generates a tree-like array of children of a given paragraphs instance.
   *
   * @param \stdClass $instance_object
   *   A paragraph's instance info object, as retrieved from
   *   ::getParagraphFieldsInstances().
   *
   * @return array
   *   An array of arrays, where each item has a 'bundle' and 'children' keys,
   *   if that paragraph has a field that references other paragraph types.
   */
  private function getParagraphsInstanceChildren($instance_object) {
    $result = [];
    $data = $instance_object->data;

    if (!isset($data['settings']['allowed_bundles'])) {
      return ['bundle' => $instance_object->bundle];
    }

    $allowed = [];
    foreach ($data['settings']['allowed_bundles'] as $key => $value) {
      if ($key == $value && !in_array($value, $allowed)) {
        $allowed[] = $value;
      }
    }
    foreach ($allowed as $bundle) {
      $item = [
        'bundle' => $bundle,
      ];

      if (isset($this->tree[$bundle])) {
        $item['children'] = $this->tree[$bundle];
      }
      else {
        $item['children'] = $this->getParagraphsInstanceChildren($this->paragraphsFields[$bundle]);
        $this->tree[$bundle] = $item['children'];
      }
      $result[] = $item;
    }
    return $result;
  }

  /**
   * @param \Drupal\Core\Database\Connection $connection
   *
   * @return mixed
   */
  protected function getParagraphFieldsInstances(\Drupal\Core\Database\Connection $connection) {
    $instances = $connection->query("SELECT fci.field_name, fci.entity_type, fci.bundle, pb.`name`, fci.data FROM field_config_instance fci
      LEFT JOIN paragraphs_bundle pb ON fci.bundle = pb.bundle
      INNER JOIN field_config fc ON fci.field_id = fc.id
      WHERE fc.`module` = 'paragraphs' AND fci.entity_type IN ('paragraphs_item', 'node')")
      ->fetchAllAssoc('bundle');

    foreach ($instances as $instance) {
      $instance->data = unserialize($instance->data);
    }

    return $instances;
  }

}
