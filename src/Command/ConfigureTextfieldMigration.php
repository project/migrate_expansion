<?php

namespace Drupal\migrate_expansion\Command;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drush\Commands\DrushCommands;
use League\CLImate\CLImate;

/**
 * Class ConfigureTextfieldMigration.
 */
class ConfigureTextfieldMigration extends DrushCommands {

  /**
   * Drupal's config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Climate instance.
   *
   * @var \League\CLImate\CLImate
   */
  private $climate;

  /**
   * ConfigureTextfieldMigration constructor.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
    $this->climate = new CLImate();
  }

  /**
   * Configures textfield migration behavior for a D7 source.
   *
   * Current coverage:
   *  - "Conflicting text processing settings in Drupal 7".
   *
   * @param string $legacy_db_key
   *   The filename of the json file, under ukids_migration/data.
   *
   * @command migrate_expansion:configure-textfield-migration
   *
   * @see https://www.drupal.org/docs/8/upgrade/known-issues-when-upgrading-from-drupal-6-or-7-to-drupal-8#plain-text
   */
  public function configureTextfieldMigration($legacy_db_key) {
    $connection = Database::getConnection('default', $legacy_db_key);

    $multi_instance_fields = $this->getFieldsWithMultipleInstances($connection);

    // Go through each field name.
    $plain_and_filtered_fields = [];

    // @todo: Add per-bundle analysis.
    foreach (array_keys($multi_instance_fields) as $field_name) {
      $instances = $connection->query("SELECT id, field_name, bundle, data from {field_config_instance}
        WHERE field_name = :field_name", [':field_name' => $field_name])
        ->fetchAll();

      $storage_settings = [];
      foreach ($instances as $instance) {
        $instance->data = $instance->data ? unserialize($instance->data) : NULL;

        if (!empty($instance->data)) {
          if ($instance->data['settings']['text_processing'] === 0) {
            $storage_settings['plain_text'] = TRUE;
          }
          elseif ($instance->data['settings']['text_processing'] === 1) {
            $storage_settings['filtered_text'] = TRUE;
          }
        }

        if ($storage_settings['plain_text'] && $storage_settings['filtered_text']) {
          $plain_and_filtered_fields[] = $instance->field_name;
          break;
        }
      }
    }

    // Prompt the user for action to take, and store it in config.
    if ($plain_and_filtered_fields) {
      $this->configurePlainAndFilteredTextfields($plain_and_filtered_fields);
    }
  }

  /**
   * Gets the textfields from the D7 database present in more than one bundle.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The drupal 7 database connection.
   *
   * @return mixed
   *   An associative array, with each key being a field name.
   */
  private function getFieldsWithMultipleInstances(Connection $connection) {
    return $connection->query("SELECT cfgi.field_name from {field_config_instance} cfgi
      INNER JOIN {field_config} cfg ON cfgi.field_id = cfg.id
      WHERE cfg.module = 'text'
      GROUP BY cfgi.field_name
      HAVING COUNT(cfgi.bundle) > 1;")->fetchAllAssoc('field_name');
  }

  /**
   * Configures the action to take when running the textfield migration.
   *
   * @param array $plain_and_filtered_fields
   *   The array of fields that have both plain and filtered instances in D7.
   */
  private function configurePlainAndFilteredTextfields(array $plain_and_filtered_fields): void {
    $this->climate->yellow("The Following fields use both plain and filtered_text");
    $input    = $this->climate->checkboxes('Select the fields you want to migrate as filtered_text', $plain_and_filtered_fields);
    $response = $input->prompt();

    $label_number = (string) (new PluralTranslatableMarkup(count($response), ' field', ' fields'));
    $this->climate->green("OK: " . implode(", ", $response) . $label_number . ' will be migrated as filtered text.');

    $config = $this->configFactory->getEditable('migrate_expansion.settings');
    $config->set('d7_textfield_plain_and_filtered_keep_filtered', serialize($response));
    $config->save();
  }

}
