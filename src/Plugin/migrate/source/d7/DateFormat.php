<?php

namespace Drupal\migrate_expansion\Plugin\migrate\source\d7;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Extracts date type and format info for Drupal 7 date types.
 *
 * @MigrateSource(
 *   id = "migrate_expansion_date_format",
 *   source_module = "ukids_migration"
 * )
 */
class DateFormat extends SqlBase {

  /**
   * Stores a local copy of the format used for each date type in D7.
   *
   * @var array
   *   The associative array of type formats.
   */
  private $typeFormats = [];

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'type' => $this->t('Type'),
      'title' => $this->t('Title'),
      'format' => $this->t('Format'),
      'locked' => $this->t('Locked'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'type' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this
      ->select('date_format_type', 'dft')
      ->fields('dft', ['type', 'title', 'locked']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $proceed = parent::prepareRow($row);

    if (empty($this->typeFormats)) {
      $patterns_query = $this->database->select('variable', 'var');
      $patterns_query->fields('var')
        ->where('var.name LIKE \'date_format_%\'');
      $this->typeFormats = $patterns_query->execute()->fetchAllAssoc('name');
    }

    $row_format_type = $row->getSourceProperty('type');

    if (empty($this->typeFormats['date_format_' . $row_format_type])) {
      return FALSE;
    }

    $row->setSourceProperty('format', unserialize($this->typeFormats['date_format_' . $row_format_type]->value));
    return $proceed;
  }

}
