<?php

namespace Drupal\migrate_expansion\Plugin\migrate\process\d7;

use Drupal\field\Plugin\migrate\process\d6\FieldInstanceWidgetSettings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Get the field instance widget settings.
 *
 * @MigrateProcessPlugin(
 *   id = "migrate_expansion_field_instance_widget_settings"
 * )
 */
class MigrateDiscoveryFieldInstanceWidgetSettings extends FieldInstanceWidgetSettings {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($widget_type, $widget_settings) = $value;
    return $this->getSettings($widget_type, $widget_settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings($widget_type, $widget_settings) {
    // Default to core behavior first.
    if ($default = parent::getSettings($widget_type, $widget_settings)) {
      return $default;
    }

    // Go ahead with other contrib modules only if core didn't return anything.
    $settings = [
      'inline_entity_form_simple' => [
        'override_labels' => $widget_settings['type_settings']['override_labels'] ?? '',
        'label_singular' => $widget_settings['type_settings']['label_singular'] ?? '',
        'label_plural' => $widget_settings['type_settings']['label_plural'] ?? '',
      ],
    ];

    return isset($settings[$widget_type]) ? $settings[$widget_type] : [];
  }

}
